// PollyMorphTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

int GetSize(int x)
{
	return x;
}

int GetSize(std::string x)
{
	return x.length();
}

class Animal
{
public:
	virtual void MakeNoise()
	{
		std::cout << "Sound goes here" << std::endl;
	}
};

class Dog : public Animal
{
public:
	void MakeNoise() override
	{
		std::cout << "Bow Wow" << std::endl;
	}
};

class Cat : public Animal
{
public:
	void MakeNoise() override
	{
		std::cout << "Nyaa" << std::endl;
	}
};

void Stroke(Animal* a)
{
	a->MakeNoise();
}

int main()
{


	std::cout << "Size of X: " << GetSize(4) << std::endl;

	std::cout << "Size of Honkeys: " << GetSize("Honkeys") << std::endl;
	Dog Doggy;
	Cat Kitty;
	Stroke(&Doggy);
	Stroke(&Kitty);

    return 0;
}

